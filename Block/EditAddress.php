<?php
namespace Magenest\CatalogAddress\Block;
use Magento\Framework\View\Element\Template;

class EditAddress extends Template{

    protected $_collectionFactory;

    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context,$data);
        $this->_collectionFactory = $collectionFactory;
        $this->_customerSession = $customerSession;
    }

    public function getAddress(){
        $data = $this->getRequest()->getParam('id');
        $model = $this->_collectionFactory->create()->addFieldToFilter('id',$data)->getData();
        return $model;
    }

}








