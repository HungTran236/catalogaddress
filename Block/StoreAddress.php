<?php
namespace Magenest\CatalogAddress\Block;
use Magento\Framework\View\Element\Template;

class StoreAddress extends Template{

    protected $_collectionFactory;

    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context,$data);
        $this->_collectionFactory = $collectionFactory;
        $this->_customerSession = $customerSession;
    }

    public function getAddress(){
        return $this->_collectionFactory->create()->getData();
    }

    public function isLoggedin(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('\Magento\Customer\Model\Session');
        $check = $customer->getCustomer()->getId();
        if(count($check) != null){
            return true;
        }
        return false;
    }

    public function getCustomerId(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('\Magento\Customer\Model\Session');
        $check = $customer->getCustomer()->getId();
        if(count($check) != null){
            return $check;
        }
        return 0;
    }

}








