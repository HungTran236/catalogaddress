<?php
namespace Magenest\CatalogAddress\Controller\Ajax;

class Delete extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $_collectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory

    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $data = $this->getRequest()->getParam("id");
        $model = $this->_collectionFactory->create()->addFieldToFilter('id',$data)->walk('delete');
        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $resultJson->setData("Delete Successfully");
        return $resultJson;
    }
}
