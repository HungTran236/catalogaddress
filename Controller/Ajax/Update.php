<?php
namespace Magenest\CatalogAddress\Controller\Ajax;

use Magento\Checkout\Exception;
use Magento\Framework\App\ObjectManager;

class Update extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $_collectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory

    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $objectManager = ObjectManager::getInstance();
        $customer = $objectManager->create('\Magento\Customer\Model\Session');
        $customerId = $customer->getCustomer()->getId();
        $addressId = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getParams();
        $addressModel = $this->_objectManager->create('Magenest\CatalogAddress\Model\CatalogAddress');
        try{
            if($addressId){
                $addressModel->load($addressId);
            }
            unset($data['id']);
            $addressModel->addData($data);
            $addressModel->setData('customer_id', $customerId);
            $addressModel->save();
            $addressNewId = $addressModel->getData('id');
            $resultJson->setData([
                'success' => true,
                'error' => false,
                'id' => $addressNewId,
                'message' => "Save data success"
            ]);
        }catch (\Exception $e){
            $resultJson->setData([
                'success' => false,
                'error' => true,
                'message' => "Save data error"
            ]);
        }
        return $resultJson;
    }
}
