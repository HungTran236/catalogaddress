<?php
namespace Magenest\CatalogAddress\Controller\Ajax;

class Update extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $_collectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory

    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);

        $id = $this->getRequest()->getParam('id');

        $data = $this->getRequest()->getParams();

        if($id != null) { // edit if dont have id parameter
            $collections = $this->_collectionFactory->create()
                ->addFieldToFilter('id', $id);
            foreach ($collections as $item) {
                $item->setStoreName($data['name']);
                $item->setAddress($data['address']);
                $item->setPhone($data['phone']);
                $item->setEmail($data['email']);
                $item->setWebsite($data['website']);
            }
            $collections->save();
            $resultJson->setData("Edit Successfully");

        }
        else{ // create if have id parameter
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customer = $objectManager->create('\Magento\Customer\Model\Session');
            $check = $customer->getCustomer()->getId();

            $data = $this->getRequest()->getParams();
            $subscription = $this->_objectManager->create('Magenest\CatalogAddress\Model\CatalogAddress');
            $subscription->setStoreName($data['name']);
            $subscription->setAddress($data['address']);
            $subscription->setPhone($data['phone']);
            $subscription->setEmail($data['email']);
            $subscription->setWebsite($data['website']);
            $subscription->setCustomerId($check);
            $subscription->save();

            $dataBack = $subscription->getData();

            $resultJson->setData(json_encode($dataBack));
        }
        return $resultJson;
    }
}
