<?php
namespace Magenest\CatalogAddress\Controller\Subscription;

class Delete extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $_collectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory

    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $id = $this->getRequest()->getParam('id');
        $model = $this->_collectionFactory->create()->addFieldToFilter('id',$id)->walk('delete');

        $resultRedirect->setPath('catalogaddress/store/address');
        return $resultRedirect;


    }
}
