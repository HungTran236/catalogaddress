<?php
namespace Magenest\CatalogAddress\Controller\Subscription;

use Magento\Framework\Controller\ResultFactory;

class Insert extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('\Magento\Customer\Model\Session');
        $check = $customer->getCustomer()->getId();

        $data = $this->getRequest()->getParams();
        $subscription = $this->_objectManager->create('Magenest\CatalogAddress\Model\CatalogAddress');
        $subscription->setStoreName($data['name']);
        $subscription->setAddress($data['address']);
        $subscription->setPhone($data['phone']);
        $subscription->setEmail($data['email']);
        $subscription->setWebsite($data['website']);
        $subscription->setCustomerId($check);
        $subscription->save();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
