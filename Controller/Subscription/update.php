<?php
namespace Magenest\CatalogAddress\Controller\Subscription;

use Magento\Framework\Controller\ResultFactory;

class Update extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    protected $collectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress\CollectionFactory $collectionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $id = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getParams();

        $collections = $this->collectionFactory->create()
            ->addFieldToFilter('id',$id);
        foreach($collections as $item)
        {
            $item->setStoreName($data['name']);
            $item->setAddress($data['address']);
            $item->setPhone($data['phone']);
            $item->setEmail($data['email']);
            $item->setWebsite($data['website']);
        }
        $collections->save();

        $resultRedirect->setPath('catalogaddress/store/address');
        return $resultRedirect;
    }
}
