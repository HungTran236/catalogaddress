<?php
namespace Magenest\CatalogAddress\Model;
use Magento\Framework\Model\AbstractModel;
class CatalogAddress extends AbstractModel {
    public function _construct() {
        $this->_init(\Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress::class);
    }
}
