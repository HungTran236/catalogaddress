<?php
namespace Magenest\CatalogAddress\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
class CatalogAddress extends AbstractDb {
    public function _construct() {
        $this->_init('magenest_catalogaddress', 'id');
    }

}