<?php
namespace Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct() {
        $this->_init('Magenest\CatalogAddress\Model\CatalogAddress',
            'Magenest\CatalogAddress\Model\ResourceModel\CatalogAddress');
    }
}