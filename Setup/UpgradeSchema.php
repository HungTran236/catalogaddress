<?php

namespace Magenest\CatalogAddress\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '2.0.19', '<')) {

            $table = $installer->getConnection()
                ->newTable('magenest_catalogaddress')
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null, [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ],
                    'Id'
                )->addColumn(
                    'store_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null, [
                    'nullable' => false
                ],
                    'Store Name'
                )->addColumn(
                    'address',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,[
                    'nullable'=>false
                ],
                    'Address'
                )->addColumn(
                    'phone',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,[
                    'nullable'=>false
                ],
                    'Phone'
                )->addColumn(
                    'email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,[
                    'nullable'=>false
                ],
                    'Email'
                )->addColumn(
                    'website',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,[
                    'nullable'=>false
                ],
                    'Website'
                )->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,[
                    'nullable'=>false
                ],
                    'Customer Id'
                );
            $installer->getConnection()->createTable($table);
//-----------------------------------------------------------------------------------------------

        }
        $installer->endSetup();


    }
}