define(
    [
        'jquery',
        'uiComponent',
        'ko',
        'Magento_Ui/js/modal/modal'
    ], function ($, Component, ko, modal) {
        'use strict';
        return Component.extend({
                textId: ko.observable(''),
                textName: ko.observable(''),
                textAddress: ko.observable(''),
                textPhone: ko.observable(''),
                textEmail: ko.observable(''),
                textWebsite: ko.observable(''),
            initialize: function () {
                this._super();
                this.addressList = ko.observableArray(this.address);
                this.isLogin = ko.observable(this.login);
                this.customerId = ko.observable(this.customer);
            },
            initObservable: function() {
                let self =this;
                this._super();
                return this;
            },
            

            isCustomer: function(data,event)  {
                let self = this;
                    if(self.customerId() == data.customer_id ){
                        return true;
                    }
                    else {
                        return false;
                    }
            },
            deleteAddress: function (data, event) {
                let self = this;
                console.log(self);
                console.log(data);
                $.ajax({
                    url: self.deleteUrl,
                    data: {
                        id: data.id,
                    },
                    success: function (response) {
                        let position = self.addressList.indexOf(data);
                        console.log(position);
                        self.addressList.splice(position,1);
                        alert(response);
                    },
                    dataType: "json",
                    showLoader: true
                });
            },

            submitAction: function (event) {
                let self = this;
                console.log(self.addressList());
                let arrayData = $('#modal-form').serialize();
                $.ajax({
                    url: self.editUrl,
                    data: arrayData,
                    success: function (response) {
                        if(response.success){
                            //kiem tra vi tri cua address cu
                            var oldAddress = self.addressList.indexOf(
                                ko.utils.arrayFirst(ko.utils.unwrapObservable(self.addressList), function(addressObj) {
                                        return ko.utils.unwrapObservable(addressObj.id) == response.id;
                                    }
                                ));
                            //xoa address cu khoi addressList
                            self.addressList.splice(oldAddress,1);
                            //them address moi
                            var test = {id:response.id, store_name:self.textName(), address:self.textAddress(), phone:self.textPhone(), email:self.textEmail(),website:self.textWebsite(),customer_id:self.customer};
                            self.addressList.push(test);
                            //message
                            alert(response.message);
                        }
                        if(response.error){
                            alert(response.message);
                        }
                    },
                    dataType: "json",
                    showLoader: true
                });
            },

            showModal: function (data, event) {
                console.log(this);
                console.log(data);
                var self = this;
                if(typeof data == 'undefined'){
                    this.textId('');
                    this.textName('');
                    this.textAddress('');
                    this.textPhone('');
                    this.textEmail('');
                    this.textWebsite('');
                }else {
                    this.textId(data.id);
                    this.textName(data.store_name);
                    this.textAddress(data.address);
                    this.textPhone(data.phone);
                    this.textEmail(data.email);
                    this.textWebsite(data.website);
                }
                $("#popup-modal").modal("openModal");
            },

            initModal: function () {
                var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: 'Edit Address',
                    buttons: [{
                        text: 'close',
                        class: 'new-modal',
                        click: function () {
                            this.closeModal();
                        }
                    }],
                };

                $("#popup-modal").modal(options);
                $('#close-modal').on("click", function () {
                    $("#popup-modal").modal("closeModal");
                });

            },

        });
    }
);