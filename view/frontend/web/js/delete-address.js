define(
    [
        'mage/storage'
    ],
    function(storage) {
    return Component.extend({
            /** Your function for ajax call */
            myAjaxCall: function(dataToPass) {

        fullScreenLoader.startLoader();
        storage.post(
            this.deleteUrl,
            JSON.stringify(dataToPass),
            true
        ).done(
            function (response) {
                /** Do your code here */
                alert('Success');
                fullScreenLoader.stopLoader();
            }
        ).fail(
            function (response) {
                fullScreenLoader.stopLoader();
            }
        );
    }
});
}
);